package com.epam.theatermanagement.dao.impl;

import com.epam.theatermanagement.dao.EventDAO;
import com.epam.theatermanagement.dao.TicketDAO;
import com.epam.theatermanagement.dao.UserDAO;
import com.epam.theatermanagement.domain.Ticket;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.junit.Assert.*;

import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AppContextTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:dataset.xml")
@DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TestTicketDAO {
	@Autowired
	private TicketDAO ticketDAO;	
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private EventDAO eventDAO;
	
	@Test
	public void getAllTest() {
		assertNotNull(ticketDAO.getAll());
	}
	
	@Test
	public void getByIdTest() {
		long id = 1;
		assertNotNull(ticketDAO.getById(id));
	}
	
	@Test(expected = EmptyResultDataAccessException.class)
	public void removeTest() {
		Ticket ticket = ticketDAO.getById((long) 21);
		ticketDAO.remove(ticket);
		assertNull(ticketDAO.getById((long) 21));
	}
	
	@Test
	public void saveTest() {
		Ticket ticket = new Ticket();
		ticket.setDateTime(LocalDate.of(2017, 7, 18));
		ticket.setSeat(2);
		ticket.setUser(userDAO.getById((long) 49));
		ticket.setEvent(eventDAO.getById((long) 1));
		ticket = ticketDAO.save(ticket);
		
		Ticket ticketExpected = ticketDAO.getById(ticket.getId());
		assertTrue(ticket.equals(ticketExpected));
	}
}
