package com.epam.theatermanagement.dao.impl;

import com.epam.theatermanagement.dao.DiscountDAO;
import com.epam.theatermanagement.dao.UserDAO;
import com.epam.theatermanagement.domain.User;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AppContextTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:dataset.xml")
@DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TestDiscountDAO {
	@Autowired
	private DiscountDAO discountDAO;
	
	@Autowired
	private UserDAO userDAO;
	
	@Test
	public void updateTest() {
		User user = userDAO.getById((long) 52);
		final int value = 1;
		final String nameStrategy = "BirthdayStrategy";
		discountDAO.update(nameStrategy, user, value);
		assertTrue(discountDAO.getValue(nameStrategy) == value);		
	}

	@Test
	public void getValueTest() {
		assertNotNull(discountDAO.getValue("BirthdayStrategy"));
	}
}
