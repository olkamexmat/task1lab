package com.epam.theatermanagement.service.impl;

import com.epam.theatermanagement.dao.UserDAO;
import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.exceptions.DAOException;
import com.epam.theatermanagement.exceptions.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {
    @Mock
    private UserDAO userDAO;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void saveUserTest() throws ServiceException, SQLException {
        User user = new User();
        user.setFirstName("user");
        user.setLastName("new");
        user.setEmail("user@gmail.com");
        user.setBirthday(LocalDate.now());
        
        when(userDAO.save(user)).thenReturn(user);
        
        user = userService.save(user);
              
        verify(userDAO).save(user);
    }

    @Test
    public void removeUserTest() {
        User user = new User();
        userService.remove(user);
        verify(userDAO).remove(user);
    }
    
    @Test
    public void getByIdTest() {
    	long userId = 50;
    	User userExpected = new User();
    	
    	when(userDAO.getById(userId)).thenReturn(userExpected);
    	
    	User user = userService.getById(userId);
    	assertTrue(userExpected.equals(user));
    }

    @Test
    public void getAllTest() {
        List<User> usersExpected = new ArrayList<>();

        when(userService.getAll()).thenReturn(usersExpected);

        List<User> users = (List<User>) userService.getAll();
        assertTrue(usersExpected.equals(users));
    }
    
    @Test
    public void getUserByEmailTest() throws ServiceException, DAOException {
    	String userEmail = "kit_ivan@mail.ru";
    	User userExpected = new User();
    	
    	when(userDAO.getUserByEmail(userEmail)).thenReturn(userExpected);
    	
    	User user = userService.getUserByEmail(userEmail);
    	assertTrue(userExpected.equals(user));
    }
}
