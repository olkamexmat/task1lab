package com.epam.theatermanagement.service.impl;

import com.epam.theatermanagement.dao.EventDAO;
import com.epam.theatermanagement.domain.Event;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EventServiceImplTest {
	@Mock
	private EventDAO eventDao;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@InjectMocks
	private EventServiceImpl eventService;
	
	@Test
	public void saveTest() {
		Event event = new Event();
		
		when(eventDao.save(event)).thenReturn(event);
		
		event = eventService.save(event);
		verify(eventDao).save(event);
	}
	
	@Test
	public void removeTest() {
		Event event = new Event();
		eventService.remove(event);
		verify(eventDao).remove(event);
	}
	
	@Test
	public void getByIdTest() {
		long eventId = 1;
        Event eventExpected = new Event();

        when(eventDao.getById(eventId)).thenReturn(eventExpected);

        Event event = eventService.getById(eventId);
//        assertTrue(eventExpected.equals(event));
        verify(eventDao).remove(event);
	}
	
	@Test
	public void getAllTest() {
        List<Event> eventsExpected = new ArrayList<>();

        when(eventService.getAll()).thenReturn(eventsExpected);

        List<Event> events = (List<Event>) eventService.getAll();
        assertTrue(eventsExpected.equals(events));
        verify(eventDao).getAll();
	}
	
	@Test
	public void getByNameTest() {
        String eventName = "Bi-2";
        Event eventExpected = new Event();

        when(eventDao.getByName(eventName)).thenReturn(eventExpected);

        Event event = eventService.getByName(eventName);
        assert event != null;
        verify(eventDao).getByName(event.getName());
    }
}
