package com.epam.theatermanagement.exceptions;

public class ServiceException extends Exception {
	/**
	 * generated serial version
	 */
	private static final long serialVersionUID = -8304302691709039649L;

	public ServiceException() {
	}

	public ServiceException(String message, Throwable exception) {
		super(message, exception);
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable exception) {
		super(exception);
	}
}
