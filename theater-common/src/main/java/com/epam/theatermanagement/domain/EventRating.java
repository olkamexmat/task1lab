package com.epam.theatermanagement.domain;

/**
 * @author Volha_Shautsova
 */
public enum EventRating {

    LOW,

    MID,

    HIGH;

}
