package com.epam.theatermanagement.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import com.epam.theatermanagement.domain.Auditorium;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.sql.DataSource;

@Configuration
@ImportResource("classpath:theater-spring.xml")
@ComponentScan({"com.epam.theatermanagement"})
@PropertySource({"classpath:auditorium.properties"})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ApplicationConfig {
    @Autowired
    DataSource dataSource;

    @Bean
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource);
    }

    @Autowired
	private Environment environment;

	public Auditorium getFirstAuditorium(String name, long numberOfSeats, Set<Long> vipSeats) {
		Auditorium firstAuditorium = new Auditorium();
		firstAuditorium.setName(name);
		firstAuditorium.setNumberOfSeats(numberOfSeats);
		firstAuditorium.setVipSeats(vipSeats);
		return firstAuditorium;
	}

	@Bean
	public Set<Auditorium> getAuditoriumSet() {
		Set<Auditorium> auditoriums = new HashSet<>();

		String nameFirstAuditorium = environment.getProperty("firstAud.name");
		long numberSeatsFirstAuditorium = Long.valueOf(environment.getProperty("firstAud.numberOfSeats"));
		Set<Long> vipSeatsFirstAuditorium;

		vipSeatsFirstAuditorium = Pattern.compile(",").splitAsStream(environment.getProperty("firstAud.vipSeats"))
				.map(Long::parseLong).collect(Collectors.toSet());

		auditoriums.add(getFirstAuditorium(nameFirstAuditorium, numberSeatsFirstAuditorium, vipSeatsFirstAuditorium));
		return auditoriums;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
