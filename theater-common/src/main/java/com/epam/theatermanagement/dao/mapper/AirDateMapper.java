package com.epam.theatermanagement.dao.mapper;


import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class AirDateMapper implements RowMapper<Long> {
    private static final String AIR_DATE_ID = "AIR_DATE_ID";

    @Override
    public Long mapRow(ResultSet resultSet, int i) throws SQLException {
        return resultSet.getLong(AIR_DATE_ID);
    }
}
