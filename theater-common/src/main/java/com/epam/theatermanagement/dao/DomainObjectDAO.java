package com.epam.theatermanagement.dao;

import com.epam.theatermanagement.domain.DomainObject;
import com.epam.theatermanagement.exceptions.DAOException;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * @param <T>
 * @author Volha_Shautsova
 */
interface DomainObjectDAO<T extends DomainObject> {

	/**
	 * Saving new object to storage or updating existing one
	 *
	 * @param object
	 *            Object to save
	 * @return saved object with assigned id
	 * @throws DAOException 
	 */
	T save(@Nonnull T object) throws DAOException;

	/**
	 * Removing object from storage
	 *
	 * @param object
	 *            Object to remove
	 */
	void remove(@Nonnull T object);

	/**
	 * Getting object by id from storage
	 *
	 * @param id
	 *            id of the object
	 * @return Found object or <code>null</code>
	 */
	T getById(@Nonnull Long id);

	/**
	 * Getting all objects from storage
	 *
	 * @return collection of objects
	 */
	@Nonnull
	Collection<T> getAll();

}
