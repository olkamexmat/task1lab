package com.epam.theatermanagement.dao.mapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public final class DateMapper implements RowMapper<LocalDate> {
    private static final String AIR_DATE = "AIR_DATE";

    @Override
    public LocalDate mapRow(ResultSet resultSet, int i) throws SQLException {
        return resultSet.getDate(AIR_DATE).toLocalDate();
    }
}
