package com.epam.theatermanagement.dao.mapper;


import org.springframework.jdbc.core.RowMapper;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.EventRating;
import com.epam.theatermanagement.domain.Ticket;
import com.epam.theatermanagement.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class TicketMapper implements RowMapper<Ticket> {
	private static final String TICKET_ID = "TICKET_ID";
	private static final String TICKET_DATE = "TICKET_DATE";
	private static final String SEAT = "SEAT";
	
	private static final String USER_ID = "USER_ID";
	private static final String FIRST_NAME = "FIRST_NAME";
	private static final String LAST_NAME = "LAST_NAME";
	private static final String EMAIL = "EMAIL";
	private static final String BIRTHDAY = "BIRTHDAY";
	
	private static final String EVENT_ID = "EVENT_ID";
	private static final String EVENT_NAME = "EVENT_NAME";
	private static final String BASE_PRICE = "BASE_PRICE";
	private static final String RATING = "RATING";
	
    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setId(resultSet.getLong(TICKET_ID));
        ticket.setDateTime(resultSet.getDate(TICKET_DATE).toLocalDate());
        ticket.setSeat(resultSet.getLong(SEAT)); 
        
        User user = new User();
        user.setId(resultSet.getLong(USER_ID));
        user.setFirstName(resultSet.getString(FIRST_NAME));
        user.setLastName(resultSet.getString(LAST_NAME));
        user.setEmail(resultSet.getString(EMAIL));
        user.setBirthday(resultSet.getDate(BIRTHDAY).toLocalDate());
        ticket.setUser(user);
        
        Event event = new Event();
        event.setId(resultSet.getLong(EVENT_ID));
        event.setName(resultSet.getString(EVENT_NAME));
        event.setBasePrice(resultSet.getDouble(BASE_PRICE));
        event.setRating(EventRating.valueOf(resultSet.getString(RATING).toUpperCase()));
        ticket.setEvent(event);
        return ticket;
    }
}
