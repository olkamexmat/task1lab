package com.epam.theatermanagement.dao;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.epam.theatermanagement.domain.Event;

import javax.annotation.Resource;

@Repository
public class CounterDAO {
    private static final String SQL_GET_COUNTER_ID__BY_NAME =
            "SELECT COUNTER_ID FROM Counter WHERE COUNTER_NAME = ?";
    private static final String SQL_UPDATE_COUNTER =
            "UPDATE Counter_Event SET COUNTER_VALUE = ?, EVENT_ID = ? WHERE COUNTER_ID = ?";
    private static final String SQL_GET_VALUE =
            "SELECT COUNTER_VALUE FROM Counter INNER JOIN COUNTER_EVENT " +
                    "ON Counter.COUNTER_ID = COUNTER_EVENT.COUNTER_ID WHERE COUNTER_NAME = ?";
    
    @Resource(name = "getJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private int getIdCounter(String name) {
        return jdbcTemplate.queryForObject(
                SQL_GET_COUNTER_ID__BY_NAME, new Object[]{name}, Integer.class
        );
    }

    /**
     * Updating value of counter for certain event and method
     *
     * @param event Event on which method with name <code>name</code> execute
     * @param name the name of method
     * @param value new value of counter
     */
    public void update(Event event, String name, int value) {
        jdbcTemplate.update(
                SQL_UPDATE_COUNTER,
                value, event.getId(), getIdCounter(name)
        );
    }

    /**
     * Getting current counter value from table
     * @param name the name of executed method
     * @return current value of counter
     */
    public int getValue(String name) {
        return jdbcTemplate.queryForObject(
                SQL_GET_VALUE, new Object[] {name}, Integer.class
        );
    }

}
