package com.epam.theatermanagement.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatermanagement.domain.Auditorium;

import javax.annotation.Resource;
import java.util.Set;

/**
 * @author Volha_Shautsova
 */
@Component
public class AuditoriumDAO {

    @Resource(name = "getAuditoriumSet")
    private Set<Auditorium> auditoriumSet;

    @Autowired
    public AuditoriumDAO(Set<Auditorium> auditoriums) {
        auditoriumSet = auditoriums;
    }

    /**
     * @return all auditoriums in storage <code>Set<Auditorium></code>
     */
    public Set<Auditorium> getAll() {
        return auditoriumSet;
    }

    /**
     * @param name name of auditorium
     * @return auditorium
     * if no any matches @return <code>null</code>
     */
    public Auditorium getByName(String name) {
        for (Auditorium auditorium : auditoriumSet) {
            if (auditorium.getName().equals(name)) {
                return auditorium;
            }
        }
        return null;
    }


}
