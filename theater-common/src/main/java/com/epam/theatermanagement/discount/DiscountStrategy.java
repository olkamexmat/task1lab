package com.epam.theatermanagement.discount;

import java.time.LocalDate;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.User;

/**
 * @author Volha_Shautsova
 */
public interface DiscountStrategy {

    /**
     * Getting discount for certain user
     *
     * @param user which books tickets
     * @param event the certain event for which tickets booked
     *              @see com.epam.theatermanagement.domain.Event
     * @param airDateTime the date of taking place event
     * @param numberOfTickets amount of booked tickets
     * @return discount for strategy
     */
    int executeStrategy(User user,
                               Event event, LocalDate airDateTime,
                               long numberOfTickets);
}
