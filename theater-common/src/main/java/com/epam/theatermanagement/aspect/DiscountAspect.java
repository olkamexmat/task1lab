package com.epam.theatermanagement.aspect;

import com.epam.theatermanagement.dao.DiscountDAO;
import com.epam.theatermanagement.discount.BirthdayStrategy;
import com.epam.theatermanagement.discount.DiscountStrategy;
import com.epam.theatermanagement.discount.Every10thStrategy;
import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.service.impl.DiscountServiceImpl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Aspect
public class DiscountAspect {
    private static final Logger LOGGER = LogManager.getLogger(DiscountAspect.class);
    private Map<User, Map<DiscountStrategy, Integer>> counter;
    @Autowired
    private DiscountServiceImpl discountService;
    @Autowired
    private DiscountDAO discountDAO;

    @Pointcut("execution(public double com.epam.theatermanagement.service.impl.DiscountServiceImpl.getDiscount(..))")
    private void getAllTotalDiscounts() {
    }

    @AfterReturning(pointcut = "getAllTotalDiscounts()")
    public void countDiscount() {
        LOGGER.info("worked discountAspect");
        counter = DiscountServiceImpl.getUserMap();
        
        for (User user : counter.keySet()) {
            for (DiscountStrategy strategy : counter.get(user).keySet()) {
                if (strategy instanceof BirthdayStrategy) {
                    discountDAO.update("birthdayStrategy", user, counter.get(user).get(strategy));
                } else if (strategy instanceof Every10thStrategy) {
                    discountDAO.update("every10thTicketStrategy", user, counter.get(user).get(strategy));
                }
            }
        }
        LOGGER.debug(counter);
    }

    public Map<User, Map<DiscountStrategy, Integer>> getCounter() {
        return counter;
    }

    public void setCounter(Map<User, Map<DiscountStrategy, Integer>> counter) {
        this.counter = counter;
    }

}
